# Google Summer of Code 2018
The development team of the [C3G](http://computationalgenomics.ca/) Montreal node is aiming to participate as an organization in the Google Summer of Code 2018 program. We are offering to mentor students who wish to spend their summer working on interesting and user-friendly software pipelines for the analysis of Next-Generation Sequencing data. This page hosts information for prospective GSoC students and our project ideas. We are looking for 2 to 4 excellent students to collaborate on some of our research and development projects.


# Where to start:
If you are a GSoC applicant and wish to join our team for the summer, here is what you need to do:

1. Read the projects below to see which one interests you. If you have questions about a particular project, you can contact the assigned mentors directly.
2. Make sure you have the requirements needed for the project, which are listed under "Developer profile"
3. Complete the instructions in "Selection tests" and "Test results"
4. Contact the mentors for the project listed under "Mentors"
5. Take a look at [student application guideline](Student_application.md), for the requirements of the proposal


[**See GSoC 2018 full time line**](timeline.md)

--------------
Table of content:

[TOC]

--------------
# The C3G Montreal Node 
The C3G Montreal node is hosted at the McGill University and Genome Quebec Innovation Center (MUGQIC). The Montreal node is strongly involved with the [GenAP](https://genap.ca/) development team and have developed a robust genomic data analysis pipeline set. Since 2011, we have completed more than 900 bioinformatics analysis projects with over 400 distinct groups of researchers across Canada. Our team has significant experience in personalized medicine applications. Our recent projects include genome analysis and interpretation of personal genomes, technology and services to record patient presentations, DNA/RNA/ChIP-seq data analysis, as well as analysis of complete human epigenomes in both germline disorders and cancers.

The current members of the team are:

 * Guillaume Bourque, _Ph.D_ - Director
 * Mathieu Bourgey, _Ph.D_ - R&D Bioinformatics manager
 * Francois Lefebvre, _MSc_ - Services Bioinformatics manager
 * Edouard Henrion, _MSc_ - Software developer
 * David Bujold, _MSc_ - GenAP project manager
 * Emmanuel Gonzalez, _Ph.D_ - Bioinformatics specialist
 * Gary Leveque, _MSc_ - Bioinformatics specialist
 * Robert Eveleigh, _MSc_ - Bioinformatics specialist
 * Rola Dali, _Ph.D_ - Bioinformatics specialist
 * Éloi Mercier, _MSc_ - Bioinformatics Consultant
 * José Héctor Gálvez López, _MSc_ - Bioinformatics Consultant
 * Pascale Marquis, _MSc_ - Bioinformatics Consultant
 * Shao Xiaojian, _Ph.D_ - Bioinformatics Consultant
 * Lindsay Dayton, _MSc_ - Research Administrator
 * David Anderson, _B.FA_ - Web Designer
 * Romain Grégoire, Web Application Developer
 * Toby Hocking, _Ph.D_ - Postdoctoral researcher
 * Jean Monlong, _MSc_ - PhD student
 * Patricia Goerner-Potvin, _MSc_ - PhD student
 * Maxime Caron, _MSc_ - PhD student
 * Joe Su, _B.Sc_ - Master Student
 * Katherina Radan, _B.Sc_ - Master Student
 * David Venuto, Research assistant
 * Lucia Bogdan, Undergraduate student
 * Cristian Groza, Undergraduate student


Collaborators:

 * Simon Gravel, _Ph.D_ - Assistant Professor at McGill University - [Simon's lab](simongravlabel.lab.mcgill.ca/Home.html)

# Mailing list

Students will be added to the mailing list once accepted into the program.

# Student application
Selected students will be automatically added to the mailing list

Selected students should follow the [student application guideline](Student_application.md)
 

# Proposed Projects 

## Supervised ChIP-seq pipeline

[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution.

[PeakSeg](http://jmlr.org/proceedings/papers/v37/hocking15.html) is a supervised machine learning model for ChIP-seq data analysis. An [R package for genome-wide supervised ChIP-seq peak calling](https://github.com/tdhock/PeakSegPipeline) has been developed, but it does not yet support dependencies between jobs, nor a smart restart mechanism, nor SLURM.

The goal of this project is to develop a MUGQIC pipeline for supervised ChIP-seq peak calling, so that genomic researchers can more easily use this new machine learning method. Since coding the pipeline should not take more than 4 weeks, the rest of the GSOC project should be devoted to writing tests, documentation, and perhaps a blog.

### Developer profile

Required skills: Python, git.

Nice to have: R, C++.

### Selection tests

Follow the instructions on [the PeakSegPipeline README](https://github.com/tdhock/PeakSegPipeline), install the software to your local computer. Run [test-pipeline-demo.R](https://github.com/tdhock/PeakSegPipeline/blob/master/tests/testthat/test-pipeline-demo.R) what is your command line output? What does the results web page (index.html) look like? Upload the output and a screenshot of the web page to a github repo.

Study the [Pull Request that Toby has created](https://bitbucket.org/mugqic/genpipes/pull-requests/18/supervised-chipseq/diff). What steps are missing in this PR, relative to the current PeakSegPipeline GitHub repo?

What kinds of unit/regression/integration tests would you propose to make sure that the pipeline is working as intended?

### Test results

Students, post a new GitHub or Bitbucket repo with a README file that describes your test results, and add the link to your repo below. Then send an email to mentors.

### Mentors

* Toby Dylan Hocking <toby.hocking@mail.mcgill.ca> 
* Mathieu Bourgey <mathieu.bourgey@computationalgenomics.ca> 
* Edouard Henrion <edouard.henrion@computationalgenomics.ca>


## Improvements to SegAnnDB (interactive machine learning for DNA copy number data)

### Rationale

[SegAnnDB](https://github.com/tdhock/SegAnnDB) is a web app for interactive genomic segmentation of DNA copy number profiles, [published in Bioinformatics](http://bioinformatics.oxfordjournals.org/content/early/2014/02/03/bioinformatics.btu072.short). It combines previous work on data visualization, computer vision, and machine learning into a web site that uses annotated region labels to build a user-specific segmentation model for detection copy number alterations. YouTube videos explain how it works: [basic labeling](https://www.youtube.com/watch?v=BuB5RNASHjU), [labeling and exporting high-density profiles](https://www.youtube.com/watch?v=al0kk1JWsr0). However there are several limitations to the current code (1) there is no public SegAnnDB server, (2) there are almost no unit tests, and (3) it is not easy to use with [Galaxy](https://usegalaxy.org), and (4) the learning algorithm is too simple. This GSOC project aims to fix those issues.

### Coding project ideas

The main goal of this project is to create a public SegAnnDB instance on [GenAP](https://genap.ca/public/home). We will get a virtual machine on GenAP where you can log in as root and set up the SegAnnDB software. Another goal will be uploading a bunch of public data sets to this SegAnnDB instance.

- http://members.cbio.mines-paristech.fr/~thocking/data/aichi-backup.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/medulloblastoma/Nimblegen.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/medulloblastoma/snp6.tgz
- http://members.cbio.mines-paristech.fr/~thocking/data/neuroblastoma/2013/regions.csv

Another main goal will be to add unit tests for existing SegAnnDB functions, using [Pyramid recommendations](http://docs.pylonsproject.org/en/latest/community/testing.html). Ideally these tests will use a [headless browser](https://en.wikipedia.org/wiki/Headless_browser) to simulate someone using the web app (thus the Python and the JavaScript code will be tested). For example,

* when a profile is processed (plotter.db.Profile.process), test for creation of models and breakpoints in the database.
* when a profile is deleted, check for deletion of the profile and all related database objects and files (PNG scatterplots, probes.bedGraph.gz data).
* use a headless browser to create a 1breakpoint label, and then after the displayed model is updated, test to make sure there is a breakpoint rendered inside that label.
* after adding a label on the profile page (zoomed out view of several chroms), the label is present on other pages (zoomed into a single chrom, chrom viewer on a subset of a single chrom).

Another main goal will be to add an option to export data to a UCSC track hub. Right now SegAnnDB can just export as custom tracks (bedGraph and bed files), but it would be nice to also support track hubs (bigWig and bigBed files).

If there is extra time, you could try to integrate SegAnnDB with Galaxy. The idea is that users may already have their copy number data sets in Galaxy, and they may want to send them to SegAnnDB for analysis. And then after labeling and analysis on SegAnnDB, there should be an easy way for users to get their labels and models back into Galaxy. The student will need to develop a REST API so Galaxy can talk to SegAnnDB.

A final goal of this project would be for a student who is interested in machine learning. The current machine learning model implemented in [gradient_descent.py](https://github.com/tdhock/SegAnnDB/blob/master/plotter/gradient_descent.py) is un-regularized [max-margin interval regression](http://jmlr.org/proceedings/papers/v28/hocking13.html) using the square loss and two features. We could improve the accuracy by instead using L1-regularization with a larger set of features. The coding project would be to either (1) port the FISTA solver [from R code](https://github.com/tdhock/penaltyLearning/blob/master/R/IntervalRegression.R) to python, or (2) get [Max Margin Interval Trees](https://github.com/aldro61/mmit) (already implemented in python) working with SegAnnDB.

### Developer profile

The ideal student is already familiar with web development, headless browser testing, and using Galaxy.

Required skills: git, JavaScript, Python. 

Nice to have: experience with [D3](http://d3js.org/) and [Pyramid](http://www.pylonsproject.org/projects/pyramid/about) web framework.

### Test questions

Please do not contact the mentors unless you have completed at least one of the tests below. Students who complete more tests, and more difficult tests, will be preferred.

Easy: Install SegAnnDB on your local machine by following the instructions in [INSTALL.sh](https://github.com/tdhock/SegAnnDB/blob/master/INSTALL.sh) or [Abhishek's blog post](https://abstatic.github.io/2016/08/22/docker-segann/). Choose one of the data sets (tgz links) above and upload all of those data sets onto your local SegAnnDB server, using [upload_profiles.py](https://github.com/tdhock/SegAnnDB/blob/master/plotter/static/upload_profiles.py). Make a YouTube video screencast where you explain how to label some breakpoints and copy number alterations in those data sets.
Right now there is a bug in the chromosome subset viewer created by Abhishek -- try clicking on the different subsets of the chromosome -- what is the displayed model, and what should be displayed? Please comment on that in your screencast.

Medium: Set up a headless browser testing framework on your own computer, and make a screencast that shows you running [Abhishek's test cases](https://github.com/tdhock/SegAnnDB/blob/master/tests/tests.py).

Hard: Fork SegAnnDB and set up a code coverage utility ([CodeCov](https://codecov.io/), [Coveralls](https://coveralls.io/), etc) so we can monitor Python code coverage. SegAnnDB also includes C and JavaScript code. Can you figure out a way to get code coverage reports for these other code files as well?

### Test results

Students, post a new GitHub or Bitbucket repo with a README file that describes your test results, and add the link to your repo below. Then send an email to mentors.

### Mentors

- Toby Hocking <toby.hocking@mail.mcgill.ca> is the original author of SegAnnDB and will be the main mentor of this project (weekly skype calls and email support).
- David Morais <david.morais@gmail.com> is a secondary mentor who can help with Galaxy (email support).

## Flowchart creator for MUGQIC Pipelines
[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanisms if some jobs fail during pipeline execution. Job commands and parameters can be modified through several configuration files. 

We actually maintain 7 different pipelines and are currently developing 3 more. Each pipeline contains between 10 to 40 steps. The development of these pipelines are in constant evolution. Moreover, users can decide to only run a selection of steps from the pipeline. Thus having an integrated system that automatically builds the  flowchart of the steps will be a user-friendly add-on to our software. 

The goal of this project is to develop the flowchart creation engine in the pipeline python object of our software.

### Developer profile

Required skills: Python, git.

Nice to have: Experience in flowchart development.

### Selection tests
Implement a test flowchart in python for the 5 first steps of the [DNAseq pipeline](https://bitbucket.org/mugqic/mugqic_pipelines/src/master/pipelines/dnaseq/dnaseq.py).

the corresponding step are:  
 
 1. picard_sam_to_fastq       => generate fastq files from bam files
 2. trimmomatic               => clean fastq files
 3. merge_trimmomatic_stats   => merge individuals cleaning metrics
 4. bwa_mem_picard_sort_sam   => generate bam files from fastq files
 5. picard_merge_sam_files    => merge individuals bam files

Input data could be bam (incorporated at step 1 or 5) or fastq (incorporated at step 2).

Post the result of you implementation (code and folwchart) in an external repository and send us the link to it

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)


-----------------------------


## Develop a noise reduction engine for SCoNEs 
[SCoNEs](https://bitbucket.org/mugqic/scones) is a development tools in R language which aims to call Copy Number Variation in paired cancer data (whole genome sequeuncing) using a Read Depth (RD) approach. 

The specificity of SCoNEs is to incorporate the individual (biological) variations of the RD signal in order to adjust the set of paramter used by the calling algorithm. This apporach can suffers from the presence of a technical noise in the signal that could in a certain case leads to on overfitting of the RD signal. 

The goal of the project is to integrate an object that could be use to reduce the noise of the RD signal. We are acutally thinking about using a machine learning approach but we are open to any other efficient startegy.

### Developer profile

Required skills: R, git.

Nice to have: Experience in Cancer Genomics, Statistics, Machine Learning or Noise Reduction.

### Selection tests
Implement a basic noise reduction function in R for logRatio signal of the 2 samples contained in the [test data](data_test/SCONES_test.tsv) file.
Please motivated the choice of your method and implementation by adding comments in your code. 

Post the result of you implementation in an external repository and send us the link to it.



### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>)

[Toby Hocking](<tdhock5@gmail.com>)



--------------------------------- 

## Develop add-ons for SCoNEs
[SCoNEs](https://bitbucket.org/mugqic/scones) is a development tools in R language which aims to call Copy Number Variation in paired cancer data (whole genome sequeuncing) using a Read Depth (RD) approach. The specificity of SCoNEs is to incorporate the individual (biological) variations of RD signal in order to adjust the set of paramter of the calling algorithm. The actual SCoNEs project is still in developpement and several add-ons are still to be implemented. 

The goal of the project is  to integrate two majors developpements:
   
    1 To implement a visualization engine (interactive genomic viewer) which could be use to manually explore the results
    2 To write the software documentation.

Addtionaly other add-on sugestions and developements from the student will be welcome.

### Developer profile

Required skills: R, git.

Nice to have: Experience in Cancer Genomics and statistics.

### Selection tests
Implement a basic viewer function in R for logRatio signal of the 2 samples contained in the [test data](data_test/SCONES_test.tsv) file.
Please motivated the choice of your method and implementation by adding comments in your code. 

Post the result of you implementation in an external repository and send us the link to it.

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>)

[Jean Monlong](<jean.monlong@MAIL.MCGILL.CA>)

---------------------------------

## Population genetics simulation and modelling using simulations and deep learning

The goal of this project is to improve software used to simulate evolution and interpret sequencing data. There are opportunities at multiple levels, ranging from algorithm development to data manipulation, code optimization, and user interface. One sub-project involves implementing a neural network approach to improve our ability to simulate data that matches experimental observations.

### Developer profile

Required skills: Python, git.

Nice to have: c++, statistics/math/physics/machine learning/modelling.

### Selection tests


Fill in the ellipses (...) in the ipython notebook found [here](http://simongravel.lab.mcgill.ca/software/wright_fisher-hints.ipynb), run the code, and submit a pdf containing the figures you generated. The code is tested on the anaconda distribution of python 3.

Alternatively, write your own simulator for the evolution of allele frequencies in a population.


### Mentors
[Simon Gravel](<gravellab@gmail.com>) 




---------------------------------


## Making GenAP_pipes useful on diverse platforms 

Human genomics research faces many data challenges such as volume and velocity, but also privacy.  We want to allow researchers to access large amounts of data to perform as meaningful studies as possible, bringing health research benefits to as many as possible. But we must also protect individual privacy, and allow individual hospitals and jurisdictions to guard the data that has been entrusted to them.


One cross-country project C3G is involved with, [CanDIG](http://www.distributedgenomics.ca), aims to help unlock genomic data in multiple sites by allowing researchers to send the computations to the data rather than bringing lots of data together centrally, allowing each institution to ensure that data under its care is safeguarded while still allowing researchers to perform analyses on the data and monitoring the results that are returned.


But for such an approach to work, there has to be a standardized “intermediate representation” of an analysis pipeline that can be sent to the compute resources, and a way of describing and/or packaging the tools that will be run on the data.  One such approach is the [Common Workflow Language](http://www.commonwl.org) which describes the workflow and individual tools in simple yaml files so that different backends can run the pipeline on cloud environments using docker images, or research centre batch HPC clusters with pre-installed binaries, or tested on laptops using the same representation.


C3G has a library of extremely well tested and characterized genomics analysis pipelines, the [GenAP pipelines](https://bitbucket.org/mugqic/mugqic_pipelines), which are currently expressed in a Python framework.  The framework currently generates scripts which execute the pipelines on a particular batch cluster scheduler.  These curated analyses pipelines could be more easily used across a variety of services - cloud, other HPC services, and across the CanDIG network - if the python framework emits a cross-platform representation of the pipeline such as CWL instead of or in addition to the existing output.


The final deliverable of this project will be an updated version of the GenAP Python framework with support for CWL.  The submission and execution of jobs will be tested on multiple HPC clusters as well as several cloud services.  At the end of this project, the participant will have significant experience with Python, batch execution on clusters, and workflow languages, and running bioinformatics pipelines; existing experience with at least two would be very valuable (“workflow languages” could include such things as Makefiles or other build systems).


As a Canada-wide initiative, the student will have a number of mentors available, with expertise in each area of the project.  On the CanDIG side, covering CWL and a variety of execution environments, will be Jonathan Dursi and Richard de Borja; from C3G and GenAP will be Rola Dali, who has extensive expertise and experience with the pipelines and the current framework.

### Developer profile

Required skills: Python, git, bash shell and unix-like environment, a workflow description language or tool (Makefiles, CMake, Gradle, Ruffus, or something else)

Nice to have: HPC batch queuing systems, CWL, running data analysis pipelines

### Mentors

[Jonathan Dursi](<Jonathan.Dursi@sickkids.ca>)

[Richard de Borja](<Richard.deBorja@uhnresearch.ca>)

[Rola Dali](<rola.dali@computationalgenomics.ca>)

[Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)


--------------------------------- 


## batchtools for Compute Canada

The goal of this project is to create a package extending the [batchtools](https://github.com/mllg/batchtools) and [future-batchtools](https://github.com/HenrikBengtsson/future.batchtools) R packages for seamless use on multiple [Compute Canada systems](https://www.computecanada.ca/research-portal/accessing-resources/available-resources/).


We believe these packages offer a promising path to making parallel computations more accessible to the average researcher and accelerate development for their projects. One example application from our group is found in the [PopSV project](https://github.com/jmonlong/PopSV).
We envision a scenario where running parallel R jobs would simply be a matter of loading the said extension library and submitting computation jobs through an apply-style function, that is without the need for paying attention to either cluster setup or batchtools configurations.
Even though the authors of batchtools have gone to great lengths at making configuring and using their package quite easy, doing so still requires a time investment on the part of average users, which may discourage them using it or appreciating its potential. It would therefore be beneficial to the community if a single developer went through the process once and make the fruits of his or her efforts publically available.
Deliverables for this project would be 1) an R package published on github, 2) usage documentation aimed at intermediate R users.

### Developer profile

The project can be completed under three months by a candidate with interest in learning more about R package development and some past experience as a user of workload managers like Slurm or Moab.

A candidate with extensive pre-existing experience in R development and HPC systems will likely be able to complete the project within a few weeks. Such a candidate would have the opportunity of taking the project beyond basic deliverables.

### Selection tests

Push a package to github which prints the items below (assuming a CentOS-like system and the Slurm  scheduler) to the console upon loading with library(). 

*Host and DNS domain names

*The number of cores available on the login node

*Plots a simple ggplot2 stacked bar chart of the total number of jobs per status (running, pending, etc.) in queue, or something else useful.


### Mentors

[François Lefebvre](<francois.Lefebvre@computationalgenomics.ca>)

[Emmanuel Gonzalez](<emmanuel.gonzalez@computationalgenomics.ca>)

[Jean Monlong](<jean.monlong@MAIL.MCGILL.CA>)
